function searchPlat(plats,platId)
{
	for (var i = 0; i < plats.length; i++) { if( plats[i]._id == platId ){ return i} }

	return false;
}	

Template.caisse.rendered = function ()
{ 
	Session.set('prixCommande', 0 );
	

	Session.set('listePlatFromCategorie', [] );
}

Template.caisse.helpers({

	prixCommande : function()
	{ 
		if( !_.isEmpty(Session.get("actualCommande") ))
		{
			var liste = Session.get("actualCommande").Com_plats;
			
			var total = total_commande_tva(liste);

			return numeral(total).format("0.00");
		}
	},

	date : function()
	{
		return moment().format("DD/MM/YYYY");
	},

	prixHT : function()
	{
		if( !_.isEmpty(Session.get("actualCommande") ))
		{
			var liste = Session.get("actualCommande").Com_plats;

			var total = 0;

			for (var i = 0; i < liste.length; i++) {
				
				total += liste[i].Pla_prix * liste[i].Pla_quantity;
			}

			return numeral(total).format("0.00");
		}
	},

	nombreArticle : function()
	{
		var plats = Session.get("actualCommande").Com_plats || [];

		var total = 0
		
		for (var i = 0; i < plats.length; i++)
		{
			total += plats[i].Pla_quantity;
		}

		return total;
	},

	nomTable : function()
	{
		return !_.isEmpty(Session.get("actualCommande")) ? Session.get("actualCommande").Com_table.Tab_name : "";
	},

	nombreCouverts : function()
	{
		return Session.get("actualCommande").Com_nombreCouverts;
	},

	categoriesPlat : function(){ return CategoriesPlat.find({ CatPla_categorie :{ $not : "Boissons" }  }) },

	listePlat : function()  { return Session.get("actualCommande").Com_plats; },

	listePlatFromCategorie : function()
	{
		return Session.get("listePlatFromCategorie");
	},
	
	nomPlat : function()
	{
		if( this.CatPla_nom )
		{
			return this.CatPla_nom;
		}
		else
		{
			return this.Pla_nom;
		}
	},

	isDivers : function()
	{
		return this.Pla_classe ? this.Pla_classe : false;
	},

	typePlat : function()
	{
		return this.CatPla_nom ? "choiceButtonPlat" : "choicePlat"; 
	}
})

Template.caisse.events({

	"click button.choiceButtonPlat" : function(e)
	{
		e.preventDefault();

		var categorieId = $(e.target).attr("data-categorieId");

		var categorie = CategoriesPlat.findOne({_id : categorieId});

		if( categorie.sousCategories != undefined) //Si la catégorie a une sous catégorie
		{	
			Session.set("listePlatFromCategorie",CategoriesPlat.find({CatPla_categorie : categorie.CatPla_nom}).fetch());
		}

		else
		{
			var plats = Plats.find({Pla_categorie : categorie }).fetch();

			var divers = { _id : categorie._id,Pla_nom : "divers", Pla_prix :0,Pla_categorie : categorie,Pla_classe : "diversClass" };
			
			plats.push(divers);
			
			Session.set("listePlatFromCategorie",plats);
		}
			
	},

	"click button.choiceCategoriePlat" : function(e)
	{
		e.preventDefault();

		var categorieId = $(e.target).attr("data-platId");

		var categorie = CategoriesPlat.findOne({_id : categorieId});

		if( categorie.sousCategories != undefined ) //Si la catégorie a une sous catégorie
		{
			Session.set("listePlatFromCategorie",categorie.sousCategories);
		}

		else
		{
			var plats = Plats.find({Pla_categorie : categorie }).fetch();

			Session.set("listePlatFromCategorie",plats);
		}		
	},

	/* Quand on click sur un plat à choisir */

	"click .choicePlat" : function(e)
	{
		e.preventDefault();

		var plat = Plats.findOne({_id : $(e.target).attr("data-platId")})
		
		var liste = Session.get('actualCommande').Com_plats;
		
		var index = searchPlat(liste,plat._id)
		
		if(  index !== false ) // CA veut dire qu'on en a trouvé un
		{
			liste[index].Pla_quantity += 1;
		}
		else
		{
			plat = _.extend(plat,
	  		{
	  			Pla_quantity : 1
	  		});

			liste.push(plat);
		}

		var aCommande = Session.get("actualCommande");
		
		aCommande.Com_plats = liste;
		
		Session.set('actualCommande',aCommande);
		
	},

	'click .payMethod' : function(e)
	{
		e.preventDefault();
		
		var ac = Session.get('actualCommande');
				
		var prix = parseFloat($("[name=calculatorInput]").val());
		
		var plat = Session.get("actualPlatDiver");
		
		plat.Pla_prix = prix;
		
		ac.Com_plats.push(plat);

		Session.set('actualCommande',ac);
		
		$("#platsDiversModal").modal("hide");
	},

	"click .validateButton" : function(e)
	{
		e.preventDefault();

		var commande  = {

			Com_plats : Session.get("actualCommande").Com_plats,

			Com_table : Tables.findOne({_id : Session.get("actualCommande").Com_table._id }),
			
			Com_nombreCouverts : Session.get("actualCommande").Com_nombreCouverts,
		};

		if( _.isEmpty( commande.Com_plats ) )
		{
			sAlert.error("Veuilez choisir au moins un platw");

			return false;
		}

		Meteor.call("addCommande",commande,function(err,res)
		{
			if( !err )
			{

				if( res == "table_doesnt_exist" )
				{
					sAlert.error("La table choisie n'existe pas");
				}
				else if( res == "table_not_dispo")
				{
					sAlert.warning("La table choisi n'est pas disponible");
				}
				else
				{
					Router.go("tables");

					sAlert.success("Commande ajoutée avec succès à la table "+Session.get("actualCommande").Com_table.Tab_name);

					closeTable();
				}
			}
			else
			{
				console.log("Une erreur s'est produite lors de l'enregistrement de la commande");

				console.log("Erreur : "+err.reason);
			}
		})
	},


	
})