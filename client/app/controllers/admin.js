Template.admin.redered = function()
{
	Session.set("plats",[]);
}
Template.admin.helpers({
	
	plats : function()
	{
		var categorie = CategoriesPlat.findOne({_id : Session.get("categorie")});
			
		var plats = Plats.find({Pla_categorie : categorie}).fetch();
		return plats;
	},

	categories : function()
	{
		return CategoriesPlat.find().fetch();
	},

	tables : function()
	{
		return Tables.find().fetch();
	}
})

Template.admin.events({

	/* Bouton  pour afficher les plats d'une catégorie */

	"click .categorie-btn" : function(e)
	{
		e.preventDefault();
		
		var id = $(e.target).attr("id");
					
		Session.set("categorie",id); 
	},

	/* Boutton pour supprimer un plat */

	"click .plat-btn" : function(e)
	{
		e.preventDefault();

		var plat = Plats.findOne({_id : $(e.target).attr("id")});
				
		Session.set("deletePlat",plat);
	}
})