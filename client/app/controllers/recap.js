Template.recap.helpers({
	
	commandes : function()
	{
		return Commandes.find().fetch();
	},

	Com_date : function()
	{
		return moment(new Date(this.Com_dateCreation)).format("DD/MM/YYYY hh:mm");
	}

})