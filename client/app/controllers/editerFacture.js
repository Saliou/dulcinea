Template.editerFacture.rendered = function()
{
	
	$('#editerFacture').on('show.bs.modal', function (e)
	{
		
	})
}

Template.editerFacture.helpers({

	peopleButtons : function()
	{
		var buttons = [];

		for (var i = 1; i <= 20; i++)
		{
			buttons[i] = { num : i}
		}
		return buttons;
	},
});

Template.editerFacture.events({

	'click .peopleNumber' : function(e)
	{
		$(".peopleNumber").removeClass("active");

		$(e.target).addClass("active");
	},

	'click .tvaValue' : function(e)
	{
		$(".tvaValue").removeClass("active");
		
		$(e.target).addClass("active");
	},

	"click .printFactureBtn" : function(e)
	{
		e.preventDefault();

		var montant = parseFloat($("#facturePriceInput").val());
		
		var tva = parseInt( $(".tvaValue.active").attr("data-value") );

		var nombreCouvert = parseInt( $(".peopleNumber.active").attr("data-value") );

		check(montant,Number);

		if( _.isNaN(nombreCouvert) )
		{
			sAlert.error("Veuillez choisir un nombre de couvert");

			return false;
		}
		if( _.isNaN(montant) || montant == 0 )
		{
			sAlert.error("Veuillez entrer un prix correcte");

			return false;
		}

		var toPrint = { Com_nombreCouverts : nombreCouvert, Com_prix : montant ,Com_tva : tva ,Com_edited : true,Com_plats : [] }
		
		Session.set("commandeToPrint",toPrint );
					
		setTimeout(function() {window.print();}, 200);

	},
	
})
