function searchPlat(plats,platId)
{
	for (var i = 0; i < plats.length; i++)
	{
		if( plats[i]._id == platId ){ return i}
	}

	return false;
}

Template.onePlat.helpers({

	notPresent : function()
	{
		return true;
	},
	prixU : function()
	{
		return numeral(this.Pla_prix).format("0.00");
	},

	Pla_total : function()
	{
		return numeral( this.Pla_quantity * this.Pla_prix ).format("0.00") ;
	},

})

Template.onePlat.events({

	"click .platDeleter td" : function(e)
	{	
		e.preventDefault();

		var plat = Plats.findOne({_id : $(e.target).parent().attr("data-platId")});
		
		var liste = Session.get('actualCommande').Com_plats;
		
		var index = searchPlat(liste,plat._id)
		
		if(  index !== false ) // Ca veut dire qu'on en a trouvé un
		{
			if( liste[index].Pla_quantity <= 1 )
			{
				liste = _.without(liste,liste[index]);
			}
			else
			{
				liste[index].Pla_quantity -= 1;
			}	
		}
		var ac = Session.get("actualCommande");
		
		ac.Com_plats = liste;
		
		Session.set('actualCommande',ac);
	}
})
