Template.calculator.rendered = function ()
{ 
	Session.set("calculatorTotal",undefined);	
}

Template.calculator.helpers({

	numbers : function()
	{
		return [[7,8,9],[4,5,6],[1,2,3],["c",0,"."]];

	},
	hasFloat : function()
	{
		var n = parseFloat(Session.get("calculatorTotal"));

    	var result = Number(n) === n && n % 1 !== 0;

    	return result ? "desactived" : "";
	},

	calculatorTotal:function()
	{
		return Session.get("calculatorTotal") == undefined || Session.get("calculatorTotal") == "" ? 0 : parseFloat(Session.get("calculatorTotal")) ;
	}
		
})

Template.calculator.events({

	"click .chiffreTap" : function(e)
	{
		e.preventDefault();
		
		var actualValue = $(e.target).attr("data-value");

		if( actualValue == "C" )
		{
			Session.set("calculatorTotal","");
		}

		else
		{
			if( actualValue == "." )
			{
				if( $(e.target).hasClass("desactived") ){ return false}
			}

			var actualString = Session.get("calculatorTotal") == undefined ? "" : Session.get("calculatorTotal") ;
			
			actualString = actualString+""+actualValue;

			Session.set("calculatorTotal",actualString);
		}
	}

	
})