Template.facture.onCreated(function()
{
	Session.set("commandeToPrint",Commandes.findOne({_id : "dLfgNuDMmQ2GfqmTq" }));
});

Template.facture.helpers({
	
	commande : function()
	{
		return Session.get("commandeToPrint");
	},
	dateTicket : function()
	{
		return moment().format("DD MMM YYYY");
	},
	nomTable : function()
	{
		var commande = Session.get("commandeToPrint");
		
		return commande.Com_table.Tab_name;
	},
	nombreCouverts : function()
	{
		var commande = Session.get("commandeToPrint");

		return commande.Com_nombreCouverts;
	},
	typeCouvert : function()
	{
		var commande = Session.get("commandeToPrint");
		
		return commande.Com_edited ? "Repas" : "Couvert";
	},

	notEdited : function()
	{
		return !Session.get("commandeToPrint").Com_edited;
	},
	
	/* Boucle des plats */

	total : function()
	{
		return numeral(this.Pla_quantity * this.Pla_prix).format("0.00");
	},
	prixFormated : function()
	{
		return numeral(this.Pla_prix).format("0.00");
	},
	/*Fin boucle plat */
	paiementMethods : function()
	{
		var commande = Session.get("commandeToPrint");

		return commande.Com_facture.Fac_paiements;
	},
	discount : function()
	{
		if( Session.get("discount") )
		{
			var d = Session.get("discount");

			return {taux : d.taux,price : numeral(d.reduce).format("0.00")}
		}
		else
		{
			return false;
		}
	},

	prixFinal : function()
	{	
		var commande = Session.get("commandeToPrint");
		
		var total = total_commande_tva(commande.Com_plats,false);

		if( Session.get("discount") )
		{ 
			var d = Session.get("discount");

			total -= d.reduce;
		}

		if( commande.Com_edited )
		{
			var prix = commande.Com_prix + ( commande.Com_prix * (commande.Com_tva/100) );

			return numeral(prix).format("0.00");
		}
		else
		{
			return  numeral(total).format("0.00");
		}

	},

	couvertMoyen : function()
	{
		var commande = Session.get("commandeToPrint"); 
		
		if( commande.Com_edited )
		{
			var total = commande.Com_prix + ( commande.Com_prix * (commande.Com_tva/100) );
		}
		else
		{
			var total = total_commande_tva(commande.Com_plats,false);
		}

		return numeral((total / commande.Com_nombreCouverts)).format('0.00');
	},

	infosPaiements: function()
	{
		var commande = Session.get("commandeToPrint");
		
		var total = total_commande_tva(commande.Com_plats);
		

		if( commande.Com_edited )
		{
			if( commande.Com_tva == 10 )
			{
				var htN = commande.Com_prix;
				
				var priceTvaN = commande.Com_prix * (commande.Com_tva/100);
				
				var htA = 0;
				
				var priceTvaA = 0;
			}
			else
			{
				var htN = 0;
				
				var priceTvaN = 0;
				
				var htA = commande.Com_prix;
				
				var priceTvaA = commande.Com_prix * (commande.Com_tva/100);
			}

			return [{nom : "TOTAL EUR",value : numeral(commande.Com_prix).format('0.00')},
            
            {nom : "HT TVA 10%",value : numeral(htN).format('0.00')},
            
            {nom : "TVA 10%",value : numeral(priceTvaN).format('0.00')},
            
            {nom : "HT TVA 20%",value : numeral(htA).format('0.00')},
            
            {nom : "HT 20%",value : numeral(priceTvaA).format('0.00')},
            
            ];
		}
		else
		{
			return total_commande_tva(commande.Com_plats,true);
		}
	}
})