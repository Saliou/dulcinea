function searchPlat(plats,platId)
{
	for (var i = 0; i < plats.length; i++) { if( plats[i]._id == platId ){ return i} }

	return false;
}	

Template.ajoutPlat.rendered = function ()
{ 	
	Session.set("listePlatToAdd",[]);
}

Template.ajoutPlat.helpers({

	plats : function()
	{
		return Plats.find().fetch()
	},

	categoriesPlat : function(){ return CategoriesPlat.find({ CatPla_categorie :{ $not : "Boissons" }  }) },

	listePlat : function(){ return Session.get("listePlatToAdd")},

	listePlatFromCategorie : function(){ return Session.get("listePlatFromCategorie")},
	
	nomPlat : function()
	{
		if( this.CatPla_nom )
		{
			return this.CatPla_nom;
		}
		else
		{
			return this.Pla_nom;
		}
	},

	typePlat : function()
	{
		if( this.CatPla_nom )
		{
			return "choiceButtonPlat";
		}
		else
		{
			return "choicePlat";
		}
	},

	isDivers : function()
	{
		return this.Pla_classe ? this.Pla_classe : false;
	},

	isCategorie : function()
	{
		return Session.get("listePlatFromCategorie")[0].CatPla_nom != undefined;
	}
		
})

Template.ajoutPlat.events({

	'click .addPlatContainer label' : function(e)
	{

		$(".addPlatContainer label").removeClass("choosen");
		
		$(e.target).addClass("choosen");

	},

	"click .choicePlat" : function(e)
	{
		e.preventDefault();

		var plat = Plats.findOne({_id : $(e.target).attr("data-platId")})
		
		var liste = Session.get("listePlatToAdd");
		
		var index = searchPlat(liste,plat._id)
		
		if(  index !== false ) // CA veut dire qu'on en a trouvé un
		{
			liste[index].Pla_quantity += 1;
		}
		else
		{
			plat = _.extend(plat,
	  		{
	  			Pla_quantity : 1
	  		});

			liste.push(plat);
		}

		Session.set('listePlatToAdd',liste);	
	},

	"click button.choiceButtonPlat" : function(e)
	{
		e.preventDefault();

		var categorieId = $(e.target).attr("data-categorieId");

		var categorie = CategoriesPlat.findOne({_id : categorieId});

		if( categorie.sousCategories != undefined) //Si la catégorie a une sous catégorie
		{	
			Session.set("listePlatFromCategorie",CategoriesPlat.find({CatPla_categorie : categorie.CatPla_nom}).fetch());
		}

		else
		{
			var plats = Plats.find({Pla_categorie : categorie }).fetch();

			var divers = { _id : categorie._id,Pla_nom : "divers",Pla_prix :0,Pla_categorie : categorie,Pla_classe : "diversClass" };
			
			plats.push(divers);

			Session.set("listePlatFromCategorie",plats);
		}
		
	},

	"click button.choiceCategoriePlat" : function(e)
	{
		e.preventDefault();

		var categorieId = $(e.target).attr("data-platId");

		var categorie = CategoriesPlat.findOne({_id : categorieId});

		if( categorie.sousCategories != undefined ) //Si la catégorie a une sous catégorie
		{
			Session.set("listePlatFromCategorie",categorie.sousCategories);
		}

		else
		{
			var plats = Plats.find({Pla_categorie : categorie }).fetch();

			Session.set("listePlatFromCategorie",plats);
		}		
	},

	'click .payMethod' : function(e)
	{
		e.preventDefault();
		
		var liste = Session.get('listePlatToAdd');
				
		var prix = parseFloat($("[name=calculatorInput]").val());
		
		var plat = Session.get("actualPlatDiver");
		
		plat.Pla_prix = prix;
		
		liste.push(plat);

		Session.set('listePlatToAdd',liste);
		
		$("#platsDiversModal").modal("hide");
	},
	
	'click .addPlatButton' : function(e)
	{
		e.preventDefault();

		var plats = Session.get("listePlatToAdd");

		var commande = Session.get("actualCommande");

		Meteor.call('addPlatsCommande',commande._id,plats,function(err,res){
			
			if( !err )
			{
				if( res == "commande_doesnt_exist" )
				{
					sAlert.error("La commande recherchée n'existe pas");

				}
				else if( res == "plat_doesnt_exist" )
				{
					sAlert.error("Le plat que voulez ajouter n'existe pas");

				}	
				else
				{
					sAlert.success("Plat correctement ajouté à la commande");

					Router.go("tables");

				}
			}
			else
			{
				sAlert.error("Une erreur est survenue lors de l'ajout du plat, veuillez réessayer plus tard");

				console.log("ajoutPlat Error : "+err.reason);

			}
		});

	}


})