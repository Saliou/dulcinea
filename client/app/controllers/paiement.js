function searchPlat(plats,platId)
{
	for (var i = 0; i < plats.length; i++)
	{
		if( plats[i]._id == platId ){ return i}
	}

	return false;
}	

Template.paiement.rendered = function ()
{ 	
	var actualTable = Tables.findOne({_id : Session.get("actualCommande").Com_table._id });

	Session.set('listePlatReserved', Commandes.findOne({_id :actualTable.Tab_reserved }).Com_plats );

	Session.set("paidsMethods",[]);
	
	Session.set("toPaid", numeral(total_commande_tva( Session.get("actualCommande").Com_plats )).format("0.00"));
	
	Session.set("paid",0);

	Session.set("discount",false);
}

Template.paiement.helpers({

	paimentMethods : function()
	{
		return [
			{"nom" : "Carte Bleue" ,"value" : "Carte Bleue"},
			
			{"nom" : "Chèque" ,"value" : "Chèque"},
			
			{"nom" : "Espèce" ,"value" : "Espèce"},
			
			{"nom" : "Ticket Restaurant" ,"value" : "Ticket Resto"}
		]
	},

	date : function()
	{
		var actualTable = Tables.findOne({_id : Session.get("actualCommande").Com_table._id });

		var date = new Date(Session.get("actualCommande").Com_dateCreation);

		return moment(date).format("DD/MM/YYYY");
	},

	couvertMoyen : function()
	{
		var commande = Commandes.findOne({_id : Session.get("actualCommande")._id }) ;
		
		var liste = commande.Com_plats;
		
		var total = total_commande_tva(liste);

		if( Session.get("discount") )
		{
			total -= Session.get("discount").reduce; 
		}

		return numeral(total/commande.Com_nombreCouverts).format('0.00');
	},

	prixCommande : function()
	{ 
		var liste = Session.get("actualCommande").Com_plats;
		
		var total = total_commande_tva(liste);

		if( Session.get("discount") )
		{
			total -= Session.get("discount").reduce; 
		}

		return numeral(total).format('0.00');
	},

	toPaid : function()
	{
		var total = total_commande_tva( Session.get("actualCommande").Com_plats );
		
		var toPay = total - Math.abs(Session.get("paid")) 

		if( Session.get("discount") )
		{
			toPay -= Session.get("discount").reduce;
		}
		
		toPay =  Math.abs( toPay );

		return numeral(toPay).format('0.00');
	},

	paid : function()
	{
		var listePaiement = Session.get("paidsMethods");

		var total = 0 ;

		for (var i = 0; i < listePaiement.length; i++)
		{
			total += listePaiement[i].Fac_montant;
		}

		Session.set("paid");

		return numeral(Session.get("paid")).format('0.00');
	},

	gottaPay : function()
	{
		var toPay = numeral(total_commande_tva( Session.get("actualCommande").Com_plats )).format("0.00") ;

		if( Session.get("discount") ){ toPay -= Session.get("discount").reduce }
		
		return (toPay - Math.abs(Session.get("paid"))) > 0;
	},

	allIsPaid : function()
	{
		var toPay = numeral(total_commande_tva( Session.get("actualCommande").Com_plats )).format("0.00") ;

		if( Session.get("discount") ){ toPay -= Session.get("discount").reduce }
		
		return !((toPay - Math.abs(Session.get("paid"))) > 0);
	},

	prixHT : function()
	{
		if( Session.get("listePlatReserved") )
		{
			var liste = Session.get("actualCommande").Com_plats;

			var total = 0;

			for (var i = 0; i < liste.length; i++) {
				
				total += liste[i].Pla_prix * liste[i].Pla_quantity;
			}

			return  numeral(total).format('0.00');
		}
	},

	nombreArticle : function()
	{
		var total = 0;

		var plats = Session.get("actualCommande").Com_plats;
		
		for (var i = 0; i < plats.length; i++)
		{
			total +=  plats[i].Pla_quantity;
		}

		return total;
	},
	
	listePlat : function(){ return Session.get("actualCommande").Com_plats},
	
	discountObject : function()
	{
		return Session.get("discount") ? Session.get("discount")  : false;
	},

	discountObjectPrice : function()
	{
		return Session.get("discount") ? numeral(Session.get("discount").reduce).format("0.00")  : false;
	},

	laFourchetteButton : function()
	{
		return Session.get("discount") ? false : true ;
	}
		
})

Template.paiement.events({

	"click .priceSetter .payMethod" : function(e)
	{
		e.preventDefault();

		var montant = parseFloat($("#calculatorInput").val());

		var method = $("input[name=paimentMethods]:checked").val();

		check(montant,Number);

		check(method,String);

		if( montant <= 0 ) { return false }
		
		var onePaiement = { Fac_method : method , Fac_montant : montant};

		var actualPaiements = Session.get("paidsMethods");

		actualPaiements.push( onePaiement );

		var total = 0 ;

		for (var i = 0; i < actualPaiements.length; i++)
		{
			total += actualPaiements[i].Fac_montant;
		}
		
		sAlert.warning(montant+" € correctement ajoutée ");

		Session.set("paid",numeral(total).format('0.00'));

		Session.set("paidsMethods",actualPaiements);

		//$(".paimentMethodsLabel").removeClass("active");

		//$(".calculatorContainer").addClass("hidden");

		Session.set("calculatorTotal","");

	},

	"click .validateCommade" : function(e)
	{
		e.preventDefault();
		
		var actualTable = Tables.findOne({_id : Session.get("actualCommande").Com_table._id });

		var facture = {};
		
		facture.Fac_commandeId = Session.get("actualCommande")._id;

		facture.Fac_paiements = Session.get("paidsMethods");

		if( Session.get("discount") ) { facture.Fac_discount = Session.get("discount") }

		var ac = Session.get("actualCommande");

		ac = _.extend(ac, { Com_facture : facture });

		Session.set("actualCommande",ac);

		Session.set("commandeToPrint",Session.get("actualCommande") );

		Meteor.call("payCommande",facture,function(err,res){

			if( !err  )
			{
				if( res == "commande_doesnt_exist" )
				{
					sAlert.error("La commande recherchée n'existe pas");
				}

				else
				{					
					sAlert.success("Commande enregistrée et finalisée avec succès");

					window.print();

					Router.go("tables");


				}
			}
			else
			{	
				sAlert.error("Une erreur est survenue lors de la finalisation de la commande");

				console.log("finalisationCommande Error : "+err.reason);
			}
		})
	},

	"click .paimentMethodsLabel" : function(e)
	{
		//e.preventDefault();

		$(".paimentMethodsLabel").removeClass("active");
		
		$(e.target).addClass("active");

		if( $(".calculatorContainer").hasClass("hidden") ){ $(".calculatorContainer").removeClass("hidden") }
	},

	"click .lafourchetteButton" : function(e)
	{
		e.preventDefault(); //En enleve 30%

		var actualTable = Tables.findOne({_id : Session.get("actualCommande").Com_table._id });

		var commande = Commandes.findOne({_id : actualTable.Tab_reserved});

		var plats = commande.Com_plats;

		var discount = {};

		discount.total = parseFloat(total_commande_tva(plats,false));

		discount.totalGood = 0;

		discount.taux = 30;

		for (var i = 0; i < plats.length; i++)
		{
			if( plats ) // est différent de boisson et alccol
			{	
				var tva = plats[i].Pla_categorie.CatPla_nom == "Alcool" ? plats[i].Pla_prix * 0.2 : plats[i].Pla_prix * 0.1;

				discount.totalGood += ( (plats[i].Pla_prix + tva)  * plats[i].Pla_quantity) ;
			}
		}

		discount["reduce"] = (discount["totalGood"] * ( discount["taux"] / 100 ));

		Session.set("discount",discount);

	}

})