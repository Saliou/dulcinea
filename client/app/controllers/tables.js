Template.tables.rendered = function(){

	
}

Template.tables.helpers({

	x : function() { return (this.Tab_x-1) * (80+10); },

	y : function() { return (this.Tab_y-1) * (80+10); },

	buttons : function()
	{
		var buttons = [];

		for (var i = 1; i <= 10; i++)
		{
			buttons[i] = { num : i}
		}
		return buttons;
	},
	
	nonDisponible : function()
	{

		var actualTable = !_.isEmpty(Session.get("actualCommande") ) ? Tables.findOne({_id : Session.get("actualCommande").Com_table._id }) : false;

		if(!actualTable){ return false};

		return actualTable.Tab_reserved;
	},

	nombreCouvert : function()
	{
		var actualTable = Tables.findOne({_id : Session.get("actualCommande").Com_table._id });

		var commande = Commandes.findOne({_id : actualTable.Tab_reserved});

		return commande.Com_nombreCouverts ? commande.Com_nombreCouverts : 0;
	},

	totalCommande : function()
	{
		var actualTable = Tables.findOne({_id : Session.get("actualCommande").Com_table._id });

		var liste =  Commandes.findOne({_id : actualTable.Tab_reserved}).Com_plats;
		
		return total_commande_tva( liste );
	},

	allPlats : function()
	{
		var actualTable = Tables.findOne({_id : Session.get("actualCommande").Com_table._id });

		var commande = Commandes.findOne({_id : actualTable.Tab_reserved});

		return commande.Com_plats ? commande.Com_plats : [];
	},

	colorClass : function() { return this.Tab_reserved == false ? "success" : "danger"; },

	actualTable : function()
	{
		var actualTable = !_.isEmpty(Session.get("actualCommande") ) ? Tables.findOne({_id : Session.get("actualCommande").Com_table._id }) : false;

		return actualTable;
	}
})

Template.tables.events({
	
	"click .tableInfo" : function(e)
	{
		e.preventDefault();
				
		if( $(".tablePresentation").hasClass("toHide") ){ $(".tablePresentation").removeClass("toHide") }

		var table = Tables.findOne({_id : $(e.target).attr("data-tableId")});

		var commande = Commandes.findOne({ _id : table.Tab_reserved });

		commande = commande ? commande : {Com_plats : [], Com_table : table};

		Session.set("actualCommande",commande);
	},

	"click .closeTableInfo" : function(e)
	{
		e.preventDefault();
		
		if( !$(".tablePresentation").hasClass("toHide") ){ $(".tablePresentation").addClass("toHide") }

		Session.set("actualCommande",{});
	},

	"click .nmbreCouverts" : function(e)
	{
		e.preventDefault();

		if( $(".caisseModalButton").hasClass("hidden") ){ $(".caisseModalButton").removeClass("hidden") }
		
		$(".nmbreCouverts").removeClass("actif");
		
		$(e.target).addClass("actif");

		var aCommande = Session.get("actualCommande");

		aCommande.Com_nombreCouverts = $(e.target).attr("data-value");
		
		Session.set("actualCommande",aCommande);
	},

	"click .cancelCommande" : function(e)
	{
		e.preventDefault();

		var table = Tables.findOne({_id : Session.get("actualCommande").Com_table._id });

		var commandeId = table.Tab_reserved;
		
		Meteor.call("cancelCommande",commandeId,function(err,res)
		{
			if( !err )
			{
				if( res == "commande_doesnt_exist" )
				{
					sAlert.error("Cette commande n'existe pas");
				}
				else
				{	
					sAlert.success("Commande à la table "+table.Tab_name+" annulé avec succès");
					
					closeTable();
				}
			}
			
			else
			{
				sAlert.error("Une erreur s'est produite lors de l'annulation de votre commande");

				console.log("Annulation commande Erreur : "+err.reason);
			}
		})
	}
})