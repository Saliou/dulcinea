Template.platsDiversModal.rendered = function()
{
	Session.set("listePlatToAdd",[]);
	
	$('#platsDiversModal').on('show.bs.modal', function (e)
	{
		var liste = Session.get("listePlatToAdd");

  		var catId = $(e.relatedTarget).attr("data-categorieId");
		
		var categorie = CategoriesPlat.findOne({_id : catId});

		var plat = { _id : Random.id(),Pla_nom : "Divers", Pla_prix : 0,Pla_quantity : 1,Pla_categorie : categorie };
				
		Session.set("actualPlatDiver", plat );
	})
}

Template.platsDiversModal.helpers({

});

Template.platsDiversModal.events({

	
})
