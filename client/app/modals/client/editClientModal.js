Template.editClientModal.onRendered(function(){

  this.autorun(function ()
  {
    if (GoogleMaps.loaded())
    {
      $(".editAdresseInput").geocomplete({ details: "form",detailsAttribute : "data-geo" });
    }
  });
});

Template.editClientModal.helpers({

	client : function(){ return Session.get("editClient"); },

	dateNaissance : function()
	{ 
		if( !Session.get("editClient") ) return "";

		var d = new Date(Session.get("editClient").Cli_dateNaissance);

		return moment(d).format("YYYY-MM-DD")
	},
		
	homme : function()
	{
		if( !Session.get("editClient") ) return ""; 

		return Clients.findOne({_id : Session.get("editClient")._id , Cli_sexe : "m"}) ? "checked" : ""; 
	},

	femme : function()
	{ 
		if( !Session.get("editClient") ) return "";

		return Clients.findOne({_id : Session.get("editClient")._id,Cli_sexe : "f"}) ? "checked" : "" ; 
	},

	sms : function()
	{ 
		if( !Session.get("editClient") ) return "";

		return Clients.findOne({_id : Session.get("editClient")._id}).Cli_sms ? "checked" : "" ; 
	},

	newsletter : function()
	{ 
		if( !Session.get("editClient") ) return "";

		return Clients.findOne({_id : Session.get("editClient")._id}).Cli_newsletter ? "checked" : "" ; 
	},


});

Template.editClientModal.events({

	'submit .edit-client' : function(e)
	{
		e.preventDefault();

		var client =
		{
			Cli_id : Session.get("editClient")._id,

			Cli_nom : $(e.target).find("[name=nom]").val(),
			
			Cli_prenom : $(e.target).find("[name=prenom]").val(),
			
			Cli_dateNaissance : new Date($(e.target).find('#editDateNaissance').val()),
			
			Cli_sexe : $(e.target).find("[name=sexe]:checked").val(),
			
			Cli_telephone : $(e.target).find("[name=telephone]").val(),
			
			Cli_email : $(e.target).find("[name=email]").val(),
			
			Cli_adresse : $(e.target).find("[name=adresse]").val(),
			
			Cli_ville : $(e.target).find("[name=ville]").val(),
			
			Cli_codePostal : $(e.target).find("[name=codePostal]").val(),
			
			Cli_newsletter : $(e.target).find("[name=newsletter]").is(':checked'),
			
			Cli_sms : $(e.target).find("[name=sms]").is(':checked'),
		}

		Meteor.call('editClient',client,function(err,res)
		{
			if(!err)
			{
				if( res == "telephone_exist")
				{
					alert("telephone")
				}
				else if( res == "mail_exist" )
				{
					alert("mail")
				}
				else
				{
					$('#editClient').modal('hide');
				}
			}
			else
			{
				alert("edit Client Error : "+err.reason);
			}
		})
	},
	
	
})
