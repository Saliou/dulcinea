Template.deleteClientModal.helpers({

	client : function(){ return Session.get("deleteClient"); }

});

Template.deleteClientModal.events({

	'click .deleteClient' : function(e)
	{
		e.preventDefault();

		var clientId = Session.get("deleteClient")._id;

		Meteor.call('deleteClient',clientId,function(err,res)
		{
			if(!err)
			{

				$('#deleteClient').modal('hide');
				Router.go("listeClients");

			}
			else
			{
				alert("del Error : "+err.reason);
			}
		})
	}
})

