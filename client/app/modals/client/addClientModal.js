Template.addClientModal.onRendered(function(){

  this.autorun(function ()
  {
    if (GoogleMaps.loaded())
    {
      $(".addAdresseInput").geocomplete({ details: "form",detailsAttribute : "data-geo" });
    }
  });
});

Template.addClientModal.helpers({


});

Template.addClientModal.events({

	'submit .add-client' : function(e)
	{
		e.preventDefault();

		var client =
		{

			Cli_nom : $(e.target).find("[name=nom]").val(),
			
			Cli_prenom : $(e.target).find("[name=prenom]").val(),
			
			Cli_dateNaissance : new Date($(e.target).find('#editDateNaissance').val()),
			
			Cli_sexe : $(e.target).find("[name=sexe]:checked").val(),
			
			Cli_telephone : $(e.target).find("[name=telephone]").val(),
			
			Cli_email : $(e.target).find("[name=email]").val(),
			
			Cli_adresse : $(e.target).find("[name=adresse]").val(),
			
			Cli_ville : $(e.target).find("[name=ville]").val(),
			
			Cli_codePostal : $(e.target).find("[name=codePostal]").val(),
			
			Cli_newsletter : $(e.target).find("[name=newsletter]").is(':checked'),
			
			Cli_sms : $(e.target).find("[name=sms]").is(':checked'),
		}

		Meteor.call('addClient',client,function(err,res)
		{
			if(!err)
			{
				if( res == "telephone_exist")
				{
					alert("telephone")
				}
				else if( res == "mail_exist" )
				{
					alert("mail")
				}
				else
				{
					$('#addClient').modal('hide');

					Router.go("clientPage",{_id : res});
				}
			}
			else
			{
				alert("edit Client Error : "+err.reason);
			}
		})
	},
	
	
})
