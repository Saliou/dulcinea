Template.deletePlatModal.helpers({

	plat : function(){ return  Session.get("deletePlat"); }

});

Template.deletePlatModal.events({

	'click .deletePlatModalBtn' : function(e)
	{
		e.preventDefault();
		
		var platId = Session.get("deletePlat")._id;

		Meteor.call('deletePlat',platId,function(err,res)
		{
			if(!err)
			{
				sAlert.success("Plat supprimé avec succès");

				$('#deletePlat').modal('hide');
			}
			else
			{
				sAlert.error("Une erreur est survenue lors de la suppression du plat");

				console.log("deletePlat Error : "+err.reason);
			}
		})
	}
})

