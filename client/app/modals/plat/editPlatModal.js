Template.editPlatModal.helpers({

	plat : function(){ return Session.get("editPlat"); },

});

Template.editPlatModal.events({

	'submit .edit-plat' : function(e)
	{
		e.preventDefault();

		var plat =
		{
			Pla_id : Session.get("editPlat")._id,

			Pla_prix : parseFloat($(e.target).find("[name=prix]").val()),
			
			Pla_datePlat : new Date($(e.target).find("[name=datePlat]").val()),
		
		}

		//S'assurer que prix soit un nombre et date soit valide;	
		Meteor.call('editPlat',plat,function(err,res)
		{
			if(!err)
			{
				if( res == "plat_dont_exist")
				{
					alert("ahcat dont existd");
				}
				
				else
				{
					$('#editPlat').modal('hide');
				}
			}
			else
			{
				alert("edit Plat Error : "+err.reason);
			}
		})
	},
	
	
})
