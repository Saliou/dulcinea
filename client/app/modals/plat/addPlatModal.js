Template.addPlatModal.rendered = function()
{
	
}
Template.addPlatModal.helpers({

	categoriesPlat : function()
	{
		return CategoriesPlat.find().fetch();
	}

});


Template.addPlatModal.events({

	'click .categorieChoice' : function(e)
	{
		e.preventDefault();
		
		$(".categorieChoice").removeClass("active");

		$(e.target).addClass("active");
	},

	'submit .add-plat' : function(e)
	{
		e.preventDefault();
		
		var categorie = $(".categorieChoice.active").attr("data-value");

		var prix = parseFloat( $(e.target).find("[name=prix]").val() );

		var nom = $(e.target).find("[name=nom]").val(); 

		if( !categorie )
		{
			sAlert.error("Veuillez choisir une catégorie de plat"); return false;
		}

		if( _.isNaN(prix) )
		{
			sAlert.error("Veuillez entrer un prix correcte"); return false;
		}

		if( !_.isString(nom) || _.isEmpty(nom) )
		{
			sAlert.error("Veuillez entrer un nom de plat correcte"); return false;
		}

		var plat =
		{
			Pla_prix : prix,
			
			Pla_nom : nom,
			
			Pla_categorie : categorie ,
		};

		Meteor.call('addPlat',plat,function(err,res)
		{
			if(!err)
			{
				if( res == "client_dont_exist" )
				{
					alert('ddd');
				}
				else
				{
					$('#addPlat').modal('hide');
				}

			}
			else
			{
				alert("edit Plat Error : "+err.reason);
			}
		})
	},
	
	
})
