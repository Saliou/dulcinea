var $write , shift = false, capslock = false,hasFloat = false;

Template.keyboard.rendered = function(){

	Session.set("string","");

	Session.set("actualInput",false);

    shift = false;
    
    capslock = false;
}

Template.keyboard.helpers({

	text : function(){ return Session.get("string");}
})

Template.keyboard.events({

	

	"click .keyboard li" : function(e)
	{
		e.preventDefault();

		if( !Session.get("actualInput") ) { return false; }

		var $write = Session.get("string");

		var $this = $(e.target);
        
        character = $this.html();
        
        //Close keyboard

        if( $this.hasClass("none") ) {  return false}

        if( $this.hasClass("closeKeyboard") )
        {
        	Session.set("actualInput",false);
		   	
		   	Session.set("string","");

		   	$(".keyboardContainer:not(.hidden)").addClass("hidden");

        	return false;
        }

        //Mettre une virgule dans un chiffre

        if( $this.hasClass("float-point") )
        {
        	if( hasFloat ) { return false}

        	hasFloat = true;
		}

		//Reset le nombre

		if( $this.hasClass("reset-point") )
		{
		   	Session.set("string","");
			
			var input = Session.get("actualInput");

			$("#"+input).val("0");

			return false;
		}

        //Shift touch
		if ($this.hasClass('left-shift') || $this.hasClass('right-shift'))
		{
    		$('.letter').toggleClass('uppercase');
    		
    		$('.symbol span').toggle();
     
		    shift = (shift === true) ? false : true;
		    
		    capslock = false;
		    
		    return false;
		}

		//CapsLock Touch
		if ($this.hasClass('capslock'))
		{
		    $('.letter').toggleClass('uppercase');
		    
		    capslock = true;
		    
		    return false;
		}

		//Delete touch

		if ($this.hasClass('delete'))
		{		     
		    var $write = ($write.substr(0, $write.length - 1));

			var input = Session.get("actualInput");
		   	
		   	Session.set("string",$write);

			$("#"+input).val( $write );

		    return false;
		}

		//Special Caracter

		if ($this.hasClass('symbol')) character = $('span:visible', $this).html();
		
		if ($this.hasClass('space')) character = ' ';
		
		if ($this.hasClass('tab')) character = "\t";
		
		if ($this.hasClass('return')){  /*character = "\n"*/ Session.set("actualInput",false); $(".keyboardContainer:not(.hidden)").addClass("hidden")} 

		//Check si on est en maj ou pas
		
		if ($this.hasClass('uppercase')) character = character.toUpperCase();

		//On enleve la shift key si on appui sur une touche et que la touche shift est activé
		
		if (shift === true)
		{
		    $('.symbol span').toggle();
		    
		    if (capslock === false) $('.letter').toggleClass('uppercase');
		     
		    shift = false;
		}

		$write = ($write +""+ character);

		var input = Session.get("actualInput");

		$("#"+input).val( $write );

		Session.set("string",$write);

		}
})