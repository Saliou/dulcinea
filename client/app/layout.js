
Template.layout.helpers({
    
})

Template.layout.events({

    "click input[type=text]" : function(e)
    {
        e.preventDefault();

        if( $(e.target).hasClass("none-keyboard") ) { return false;}

        var id = $(e.target).attr("id");

        if( id == Session.get("actualInput") ){ return false;}

        Session.set("actualInput",id);

        Session.set("string",$(e.target).val());

        $(".keyboardContainer").addClass("hidden");

        if( $(e.target).hasClass("numeral-keyboard") )
        {
        	$(".keyboardContainer#numeralKeyboard").removeClass("hidden");

        }
        else
        {
        	$(".keyboardContainer#lettersKeyboard").removeClass("hidden");

        }

    },
})