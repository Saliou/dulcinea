Meteor.startup(function () {

    total_commande_tva = function( plats,tvaP )
    {
        tvaP = tvaP == undefined || tvaP == null ? false : tvaP;
        //TVA Alcool 20% tout le reste 10%
        var price = 0;
        
        var priceTvaA = 0;
        
        var priceTvaN = 0;

        var htA = 0;

        var htN = 0;

        var tvaA = 20; // TVA Alcool
        
        var tvaN = 10; // TVA Nourriture
        
        for (var i = 0; i < plats.length; i++)
        {
            var alcools = ["Bières","Champagne","Digestifs","Apéritifs"];

            var inArray = $.inArray( plats[i].Pla_categorie.CatPla_nom, alcools );

            if( inArray != -1 )
            {
                var tva = tvaA;

                htA += (plats[i].Pla_prix * plats[i].Pla_quantity);

                priceTvaA += ((plats[i].Pla_prix * plats[i].Pla_quantity ) /100) * tva
            }
            else
            {
                var tva = tvaN;
                
                htN += (plats[i].Pla_prix * plats[i].Pla_quantity)
                
                priceTvaN += (((plats[i].Pla_prix * plats[i].Pla_quantity )/100) * tva);
            }

            var tvaToAdd =  ((plats[i].Pla_prix )/100) * tva;

            tvaToAdd =  tvaToAdd;

            price += ( plats[i].Pla_prix + tvaToAdd ) * plats[i].Pla_quantity;

        }
        
        if( tvaP == false )
        {
            return numeral(price).format('0.00');
        }

        else
        {
            return [{nom : "TOTAL EUR",value : numeral(price).format('0.00')},
            
            {nom : "HT TVA 10%",value : numeral(htN).format('0.00')},
            
            {nom : "TVA 10%",value : numeral(priceTvaN).format('0.00')},
            
            {nom : "HT TVA 20%",value : numeral(htA).format('0.00')},
            
            {nom : "HT 20%",value : numeral(priceTvaA).format('0.00')},
            
            ];
        }
    };

    closeTable = function()
    {
        $(".tablePresentation").addClass("toHide");
                    
        Session.set("actualCommande",{});
    };
    
    sAlert.config({
        effect: 'genie',
        position: 'bottom-right',
        timeout: 3000,
        html: false,
        onRouteClose: true,
        stack: true,
        // or you can pass an object:
        // stack: {
        //     spacing: 10 // in px
        //     limit: 3 // when fourth alert appears all previous ones are cleared
        // }
        offset: 0, // in px - will be added to first alert (bottom or top - depends of the position in config)
        beep: false,
        // examples:
        // beep: '/beep.mp3'  // or you can pass an object:
        // beep: {
        //     info: '/beep-info.mp3',
        //     error: '/beep-error.mp3',
        //     success: '/beep-success.mp3',
        //     warning: '/beep-warning.mp3'
        // }
        onClose: _.noop //
        // examples:
        // onClose: function() {
        //     /* Code here will be executed once the alert closes. */
        // }
    });

});