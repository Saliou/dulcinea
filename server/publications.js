
//publish the collection as you used to do with Mongo.Collection 
 Meteor.publish("tableCollection", function(){
    return Tables.find();
 });
 
 Meteor.publish("commandeCollection", function(){
    return Commandes.find();
 });

 Meteor.publish("platCollection", function(){
    return Plats.find();
 });

 Meteor.publish("categoriePlatCollection", function(){
    return CategoriesPlat.find();
 });


