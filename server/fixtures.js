
if( Tables.find().count() <= 0 )
{
	var tables = [

	{ Tab_name : "#1", Tab_y : 1,Tab_x : 1},
	{ Tab_name : "#2", Tab_y : 1,Tab_x : 3},
	{ Tab_name : "#3", Tab_y : 1,Tab_x : 5},
	{ Tab_name : "#4", Tab_y : 1,Tab_x : 7},
	
	{ Tab_name : "#5", Tab_y : 2,Tab_x : 8},
	
	{ Tab_name : "#6", Tab_y : 3,Tab_x : 1},
	{ Tab_name : "#7", Tab_y : 3,Tab_x : 2},
	{ Tab_name : "#8", Tab_y : 3,Tab_x : 3},
	{ Tab_name : "#9", Tab_y : 3,Tab_x : 8},
	
	{ Tab_name : "#10", Tab_y : 4,Tab_x : 1},
	{ Tab_name : "#11", Tab_y : 4,Tab_x : 8},
	
	{ Tab_name : "#12", Tab_y : 5,Tab_x : 1},
	{ Tab_name : "#13", Tab_y : 5,Tab_x : 8},
	
	{ Tab_name : "#14", Tab_y : 6,Tab_x : 1},
	{ Tab_name : "#15", Tab_y : 6,Tab_x : 5},

	];

	for (var i = 0; i < tables.length; i++)
	{
		Meteor.call("addTable",tables[i],function(err,res)
		{
			if( !err )
			{

			}
			else
			{
				console.log("Une erreur s'est produite en insérant la table #"+i);

				console.log(err);
				
				console.log("\n\n");
			}
		})
		
	}
}

if( CategoriesPlat.find().count() <= 0)
{
	
	var cP = ["Entrées","Salades","Poissons/Viandes","Pizzas","Pates","Fromages","Dessert","Boissons"];
	
	var sousCategories = ["Soft","Boissons Chaudes","Apéritifs","Bières","Digestifs","Champagne","Cocktails"];

	for (var i = 0; i < cP.length; i++)
	{

		if( cP[i] == "Boissons" )
		{
			var Categorie = { CatPla_nom : cP[i] };			

			for (var j = 0; j < sousCategories.length; j++)
			{
				sousCategorie = { CatPla_nom : sousCategories[j],CatPla_categorie : cP[i]};

				Meteor.call("addCategoriePlat",sousCategorie,function(err,res)
				{
					if( !err )
					{

					}
					else
					{
						console.log("Une erreur s'est produite en insérant la categorie #"+cP[i]);

						console.log(err);
						
						console.log("\n\n");
					}
				})
			}

			Categorie.sousCategories = true;
		}
		else
		{
			var Categorie = { CatPla_nom : cP[i] };

		}
		
		Meteor.call("addCategoriePlat",Categorie,function(err,res)
		{
			if( !err )
			{

			}
			else
			{
				console.log("Une erreur s'est produite en insérant la categorie #"+cP[i]);

				console.log(err);
				
				console.log("\n\n");
			}
		})
	}
}

if(Plats.find().count() <= 0)
{
	var entrees = [

			{Pla_nom : "Antipasti",Pla_prix : 16.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Entrées"})},

			{Pla_nom : "Caprese",Pla_prix : 14.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Entrées"}) } ,

			{Pla_nom : "Bruschetta",Pla_prix : 10.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Entrées"})},

			{Pla_nom : "Carpaccio",Pla_prix : 15.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Entrées"}) },

			{Pla_nom : "Planche de charcuterie",Pla_prix : 17.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Entrées"})  },

			{Pla_nom : "Soupe Dulcinea",Pla_prix : 8.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Entrées"}) }
			
			
		];

		var salades = [

			{Pla_nom : "Grande salade",Pla_prix : 16.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Salades"}) } ,

			{Pla_nom : "Salade de pasta",Pla_prix :  16.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Salades"}) } ,

			{Pla_nom : "Salade ricotta",Pla_prix : 15.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Salades"}) } ,

			{Pla_nom : "Salade caesar",Pla_prix : 14.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Salades"}) } ,

			{Pla_nom : "Salade au chèvre",Pla_prix : 13.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Salades"}) },

			{Pla_nom : "Salade méditerraéenne",Pla_prix : 15.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Salades"})  }
			
		];

		var poissons = [

			{Pla_nom : "Entrecôte grillée",Pla_prix : 21,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Poissons/Viandes"}) },

			{Pla_nom : "Escalope de veau",Pla_prix : 19.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Poissons/Viandes"}) },

			{Pla_nom : "Escalope de veau",Pla_prix : 18.90,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Poissons/Viandes"}) },

			{Pla_nom : "Selle d'agneau grillée",Pla_prix : 18,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Poissons/Viandes"}) },

			{Pla_nom : "Filet de thon",Pla_prix : 21,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Poissons/Viandes"}) },

			{Pla_nom : "Dorade entiere",Pla_prix : 22,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Poissons/Viandes"}) },

			{Pla_nom : "Menu enfant",Pla_prix : 13,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Poissons/Viandes"}) }
	
		];

		var pizzas = [

			{Pla_nom : "Margherita",Pla_prix : 10.90,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pizzas"}) },

			{Pla_nom : "La Bresaola",Pla_prix : 17.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pizzas"}) },

			{Pla_nom : "La Regina",Pla_prix : 13.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pizzas"}) },

			{Pla_nom : "Vegetariana",Pla_prix : 14.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pizzas"}) },

			{Pla_nom : "Salmone",Pla_prix : 14.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pizzas"}) },

			{Pla_nom : "Dulcinea Frutti di marie",Pla_prix : 18.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pizzas"}) },

			{Pla_nom : "Quatro formaggi",Pla_prix : 14.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pizzas"}) },

			{Pla_nom : "Orientale",Pla_prix : 14.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pizzas"}) },

			{Pla_nom : "Tonno",Pla_prix : 14.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pizzas"}) },

			{Pla_nom : "Siciliana",Pla_prix : 15.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pizzas"}) }
	
		];


		var pates = [

			{Pla_nom : "Linguine alle vongole",Pla_prix : 19,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pates"}) },

			{Pla_nom : "Linguine aux gambas",Pla_prix : 19,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pates"}) },

			{Pla_nom : "Rigatoni",Pla_prix : 14,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pates"}) },

			{Pla_nom : "Penne",Pla_prix : 12.90,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pates"}) },

			{Pla_nom : "Rigatoni alla",Pla_prix : 14.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pates"}) },

			{Pla_nom : "Fettuccine",Pla_prix : 16.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pates"}) },

			{Pla_nom : "Tagliatelle a la crème",Pla_prix : 16.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pates"}) },

			{Pla_nom : "Tagliatelle au saumon",Pla_prix : 16.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pates"}) },

			{Pla_nom : "Tagliatelle carbonara",Pla_prix : 17,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pates"}) },

			{Pla_nom : "Pappardelle",Pla_prix : 15.90,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pates"}) },

			{Pla_nom : "Risotto au safran",Pla_prix : 15.90,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Pates"}) }
		];

		var fromages = [

			{Pla_nom : "Gorgonzola",Pla_prix : 5.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Fromages"}) },

			{Pla_nom : "Petite assiette",Pla_prix : 10,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Fromages"}) },

			{Pla_nom : "Grande assiette",Pla_prix : 15.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Fromages"}) }
	
		];

		var dessert = [

			{Pla_nom : "Tiramisu classique",Pla_prix : 8.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Dessert"}) },

			{Pla_nom : "panna cotta",Pla_prix : 8.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Dessert"}) },

			{Pla_nom : "glaces et sorbets",Pla_prix : 7.90,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Dessert"}) },

			{Pla_nom : "Salade de fruits",Pla_prix : 9.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Dessert"}) },

			{Pla_nom : "Tiramisu",Pla_prix : 9,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Dessert"}) },

			{Pla_nom : "Mousse au chocolat",Pla_prix : 7.90,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Dessert"}) },

			{Pla_nom : "Café gourmand",Pla_prix : 11,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Dessert"}) },

			{Pla_nom : "Thé ou crème",Pla_prix : 13,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Dessert"}) }
	
		];

		var aperitifs = [

			{Pla_nom : "Martini",Pla_prix : 6,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			{Pla_nom : "Amaro",Pla_prix : 6.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			{Pla_nom : "Campari",Pla_prix : 6.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			{Pla_nom : "Americano",Pla_prix : 7.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			{Pla_nom : "Porto",Pla_prix : 6,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			{Pla_nom : "Ricard",Pla_prix : 4.80,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			{Pla_nom : "Spritz",Pla_prix : 8,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			{Pla_nom : "Gin",Pla_prix : 8,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			{Pla_nom : "Vodka",Pla_prix : 7,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			{Pla_nom : "Tequila",Pla_prix : 7,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			//WHISKIES

			{Pla_nom : "J & B",Pla_prix : 7,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			{Pla_nom : "Johnny",Pla_prix : 8,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			{Pla_nom : "Chivas",Pla_prix : 8.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			{Pla_nom : "Jack Daniels",Pla_prix : 8.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			{Pla_nom : "Balvenie",Pla_prix : 10,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) },

			{Pla_nom : "Oban",Pla_prix : 12,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Apéritifs"}) }
	
		];

		var boissons_chaudes = [

			{Pla_nom : "Café",Pla_prix : 2.80,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Boissons Chaudes"}) },

			{Pla_nom : "Café crème",Pla_prix : 4.80,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Boissons Chaudes"}) },

			{Pla_nom : "Cappuccino",Pla_prix : 5,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Boissons Chaudes"}) },

			{Pla_nom : "Chocolat chaud",Pla_prix : 4.80,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Boissons Chaudes"}) },

			{Pla_nom : "Thé",Pla_prix : 4.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Boissons Chaudes"}) }
	
		];

		var bieres = [

			{Pla_nom : "Heineken",Pla_prix : 4.90,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Bières"}) },

			{Pla_nom : "Desperados",Pla_prix : 4.90,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Bières"}) },

			{Pla_nom : "Bière italienne",Pla_prix : 4.90,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Bières"}) }
	
		];
		
		var digestifs = [

			{Pla_nom : "Armagnac",Pla_prix : 8,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Digestifs"}) },

			{Pla_nom : "Cognac",Pla_prix : 8,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Digestifs"}) },

			{Pla_nom : "Fernet",Pla_prix : 8,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Digestifs"}) },

			{Pla_nom : "Get",Pla_prix : 8,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Digestifs"}) },

			{Pla_nom : "Baileys",Pla_prix : 8,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Digestifs"}) },

			{Pla_nom : "Grappa maison",Pla_prix : 6,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Digestifs"}) },

			{Pla_nom : "Limoncello",Pla_prix : 6,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Digestifs"}) },

			{Pla_nom : "Amaretto",Pla_prix : 6,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Digestifs"}) },

			{Pla_nom : "Sambuca",Pla_prix : 6,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Digestifs"}) }

		];

		var champagne = [

			{Pla_nom : "Champagne la coupe",Pla_prix : 11,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Champagne"}) },

			{Pla_nom : "Champagne la bouteille",Pla_prix : 75,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Champagne"}) },

			{Pla_nom : "Champagne de marque",Pla_prix : 90,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Champagne"}) }
	
		];

		var cocktails = [

			{Pla_nom : "Timo di Dino",Pla_prix : 12.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Cocktails"}) },
			
			{Pla_nom : "Balsamico rosso",Pla_prix :12.00 ,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Cocktails"}) },
			
			{Pla_nom : "Gin Germain",Pla_prix : 12.00,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Cocktails"}) },
			
			{Pla_nom : "Bourbon Sour",Pla_prix : 11 ,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Cocktails"}) },
			
			{Pla_nom : "Amareto Sour",Pla_prix : 10,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Cocktails"}) },
			
			{Pla_nom : "Strawbery-basil Mojito",Pla_prix : 12 ,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Cocktails"}) },
			
			{Pla_nom : "Gin-basil smash",Pla_prix : 12,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Cocktails"}) },
			
			{Pla_nom : "Adriatico",Pla_prix : 12.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Cocktails"}) },
			
			{Pla_nom : "Tropico",Pla_prix : 12.50 ,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Cocktails"}) },

			{Pla_nom : "Classique/Sans Alcool",Pla_prix : 10.00 ,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Cocktails"}) },
	
		];

		var soft = [

			{Pla_nom : "Perrier",Pla_prix : 4.50,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Soft"}) },

			{Pla_nom : "Coca Cola",Pla_prix : 4.80,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Soft"}) },

			{Pla_nom : "Orangina",Pla_prix : 4.80,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Soft"}) },

			{Pla_nom : "Schweppes",Pla_prix : 4.80,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Soft"}) },

			{Pla_nom : "Ice Tea",Pla_prix : 4.80,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Soft"}) },

			{Pla_nom : "Diabolo",Pla_prix : 4,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Soft"}) },

			{Pla_nom : "Granini",Pla_prix : 4.80,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Soft"}) },

			{Pla_nom : "Vittel 50cl",Pla_prix : 4.40,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Soft"}) },

			{Pla_nom : "Vittel 100cl",Pla_prix : 5.60,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Soft"}) },

			{Pla_nom : "St Pellegrino 50cl",Pla_prix : 4.40,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Soft"}) },

			{Pla_nom : "St Pellegrino 100cl",Pla_prix : 5.60,Pla_categorie : CategoriesPlat.findOne({ CatPla_nom : "Soft"}) }
	
		];

		var allPlats = [entrees,salades,poissons,pizzas,pates,fromages,dessert,aperitifs,boissons_chaudes,bieres,digestifs,champagne,soft,cocktails];

		for (var i = 0; i < allPlats.length; i++)
		{
			for (var j = 0; j < allPlats[i].length; j++)
			{
				Plats.insert(allPlats[i][j]);
			}
		}

}

