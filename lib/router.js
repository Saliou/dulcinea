Router.configure({

	layoutTemplate : "layout",
	
	notFoundTemplate : "404",
	
	loadingTemplate : "loading",

	waitOn : function(){ return Meteor.subscribe("categoriePlatCollection") && Meteor.subscribe("platCollection") && Meteor.subscribe("tableCollection") && Meteor.subscribe("commandeCollection") }
	
})


Router.route('/',{
	
	name : "tables",

	waitOn : function(){ return Meteor.subscribe("tableCollection") && Meteor.subscribe("commandeCollection")},

	data : function(){  return {tables : Tables.find() } },
	
	action : function()
	{
		//Session.set("actualCommande", { Com_plats : [],Com_table : {} });

		this.render();
	}
	
});

Router.route('/caisse',{
	
	name : "caisse",

	waitOn : function(){ return Meteor.subscribe("tableCollection") && Meteor.subscribe("commandeCollection")&& Meteor.subscribe("categoriePlatCollection") && Meteor.subscribe("platCollection")},

	action : function()
	{
		if( !_.isEmpty(Session.get("actualCommande")) )
		{
			this.render("caisse",{data : Session.get("actualCommande")}); // Dans actualCommande il ya la table et les couverts
		}
		else
		{
			this.redirect("/tables");
		}
	}
	
});

Router.route('/paiement',{
	
	name : "paiement",

	waitOn : function(){ return Meteor.subscribe("tableCollection") && Meteor.subscribe("commandeCollection")&& Meteor.subscribe("categoriePlatCollection") && Meteor.subscribe("platCollection")},

	action : function()
	{
		if( !_.isEmpty(Session.get("actualCommande")) )
		{
			this.render("paiement",{data : Session.get("actualCommande")}); // Dans actualCommande il ya la table et les couverts
		}
		else
		{
			this.redirect("/tables");
		}
	}
	
});

Router.route('/ajoutPlat',{
	
	name : "ajoutPlat",

	waitOn : function(){ return Meteor.subscribe("tableCollection") && Meteor.subscribe("commandeCollection")&& Meteor.subscribe("categoriePlatCollection") && Meteor.subscribe("platCollection")},

	action : function()
	{
		if( !_.isEmpty(Session.get("actualCommande")) )
		{
			this.render("ajoutPlat",{data : Session.get("actualCommande")}); // Dans actualCommande il ya la table et les couverts
		}
		else
		{
			this.redirect("/tables");
		}
	}
	
});

Router.route('/editerFacture',{
	
	name : "editerFacture",

	waitOn : function(){ return Meteor.subscribe("tableCollection") && Meteor.subscribe("commandeCollection")&& Meteor.subscribe("categoriePlatCollection") && Meteor.subscribe("platCollection")},
	
});

Router.route('/admin',{
	
	name : "admin",

	waitOn : function(){ return Meteor.subscribe("platCollection") && Meteor.subscribe("categoriePlatCollection") },

	
});

/*Router.route('/commandes',{
	
	name : "recap",

	waitOn : function(){ return Meteor.subscribe("platCollection") && Meteor.subscribe("categoriePlatCollection") && Meteor.subscribe("commandeCollection") },

	
});*/

Router.route('/facture/',{
	
	name : "facture",

	waitOn : function(){ return Meteor.subscribe("commandeCollection")  },

	data : function(){ return Commandes.findOne({}) },
	
});
