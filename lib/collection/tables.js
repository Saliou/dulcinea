Tables = new Meteor.Collection("table");

//ID : string
//Open : bool
//Capacity : int
//reserved : bool

Meteor.methods({

	addTable : function( table )
	{
		check(table,
		{
			Tab_name : String,

			Tab_x : Number,
			
			Tab_y : Number,
		});

		var tableToInsert = _.extend(table,
		{
			Tab_dateCreation : new Date(),

			Tab_reserved : false,
		});

		return Tables.insert(tableToInsert);
	},

	editTable : function( table )
	{
		check(table,
		{
			_id : String, 
			
			Tab_name : String,
			
			Tab_capacity : Number,
			
			Tab_open : Boolean,
			
			Tab_dateCreation : new Date,
			
			Tab_Reserved : Boolean,
		});

		var tableExist = Tables.findOne({_id : table._id});

		if( !tableExist ) { return "table_doesnt_exist"; }

		return Tables.update(table._id,{ $set : table });

	},	


	deleteTable : function( tableId )
	{
		check(tableId,String);

		var hasTable = Tables.findOne({_id : tableId});

		if( !hasTable ){ return "table_doesnt_exist";}

		return Tables.remove(hasTable);
	},
})