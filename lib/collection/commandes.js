Commandes = new Meteor.Collection("commande");

function searchPlat(plats,platId)
{
	for (var i = 0; i < plats.length; i++)
	{
		if( plats[i]._id == platId ){ return i}
	}

	return false;
}	

Meteor.methods({

	addCommande : function( commande )
	{
		/*check(commande,
		{
			Com_plats : [Object],
			
			Com_table : String,
		})*/
		

		//vérifier que la tables n'est pas occupée

		var dispoTable = Tables.findOne({_id : commande.Com_table._id});

		if( !dispoTable ){ return "table_doesnt_exist"};

		if( dispoTable.Tab_reserved != false ) { return "table_not_dispo"}

		var price = 0;

		var tvaA = 20; // TVA Alcool
		
		var tvaN = 10; // TVA Nourriture

		//TVA Alcool 20% tout le reste 10%

		for (var i = 0; i < commande.Com_plats.length; i++)
		{
			if( commande.Com_plats[i].Pla_categorie.CatPla_nom == "alcool" )
			{
				var tva = tvaA;
			}
			else
			{
				var tva = tvaN;
			}
			
			var tvaToAdd = 	((commande.Com_plats[i].Pla_prix * commande.Com_plats[i].Pla_quantity)/100) * tva;

			price += commande.Com_plats[i].Pla_prix + tvaToAdd;

		}

		price =  numeral(price).format("0.00");

		var commandeToInsert = _.extend(commande,
		{
			Com_dateCreation : new Date(),
			Com_prix : price,
			Com_paid : false,
		});

		var commandeId = Commandes.insert(commandeToInsert);

		Tables.update(commande.Com_table._id,{$set : {Tab_reserved : commandeId }});

		return commandeId;
	},

	editCommande : function( commande )
	{
		
	},

	deleteCommande : function( commandeId )
	{
		check(commandeId,String);

		var hasCommande = Commandes.findOne({_id : commandeId});

		if( !hasCommande ){ return "commande_doesnt_exist";}

		return Commandes.remove(hasCommande);
	},
	
	cancelCommande : function( commandeId )
	{
		check(commandeId,String);

		var hasCommande = Commandes.findOne({_id : commandeId});

		if( !hasCommande ){ return "commande_doesnt_exist";}

		Tables.update(hasCommande.Com_table._id,{$set : {Tab_reserved : false}});

		return Commandes.remove(hasCommande);
	},

	addPlatsCommande : function( commandeId,plats )
	{
		check(commandeId,String);

		check(plats,[Object]);
		
		var actualCommande = Commandes.findOne({ _id : commandeId });

		if( !actualCommande ){ return "commande_doesnt_exist";}
		
		var liste = actualCommande.Com_plats;

		for (var i = 0; i < plats.length; i++)
		{		
			var index = searchPlat(liste,plats[i]._id);

			if(  index !== false ) // CA veut dire qu'on en a trouvé un
			{
				liste[index].Pla_quantity += plats[i].Pla_quantity;
			}
			else
			{
				plats[i] = _.extend(plats[i],
		  		{
		  			Pla_quantity : plats[i].Pla_quantity,
		  		});
			}

			liste.push(plats[i]);	
		}

		return Commandes.update(commandeId,{$set : {Com_plats : liste }});
	},

	payCommande : function( facture )
	{ 
		/*check(facture,
		{
			Fac_commandeId : String,

			Fac_paiements : [{ Fac_method : String , Fac_montant : Number}]
		})*/

		var commandeExist = Commandes.findOne({_id : facture.Fac_commandeId});

		if( !commandeExist ){ return "commande_doesnt_exist"}

		var factureToAdd = _.omit(facture,'Fac_commandeId');
		
		factureToAdd = _.extend(factureToAdd,
		{
			Fac_dateCreation : new Date(),
		})

		Tables.update(commandeExist.Com_table._id,{$set : { Tab_reserved : false}});

		return Commandes.update(facture.Fac_commandeId,{$set : {Com_paid : true, Com_facture : factureToAdd }})

	},


})