Plats = new Meteor.Collection("plat");

Meteor.methods({

	addPlat : function( plat )
	{
		check(plat,
		{
			Pla_prix : Number,
			Pla_nom : String,
			Pla_categorie : String
		})

		var platExist = Plats.findOne({Pla_nom : plat.Pla_nom});

		var categorieExist = CategoriesPlat.findOne({_id : plat.Pla_categorie});

		if(  !categorieExist){ return "categorie_doesnt_exist"}
		
		if( platExist ){ return "plat_exist" }

		plat.Pla_categorie = categorieExist;
	
		platToInsert = _.extend(plat,
		{
			Pla_dateCreation : new Date(),
		})

		return Plats.insert(platToInsert);
	},

	editPlat : function( plat )
	{
		check(plat,
		{
			Pla_id : String,
			Pla_prix : Number,
			Pla_nom : String,
			Pla_categorie : String
		})

		var platExist = Plats.findOne({Pla_nom : plat.Pla_nom});

		var categorieExist = CategoriesPlat.findOne({_id : plat.Pla_categorie});

		if(  !categorieExist){ return "categorie_doesnt_exist"}
		
		if( platExist ){ return "plat_exist" }

		return Plats.update(plat.Pla_id,{$set : plat} );	
	},

	deletePlat : function( platId )
	{
		check(platId,String);

		var hasPlat = Plats.findOne({_id : platId});

		if( !hasPlat ){ return "plat_doesnt_exist"}

		return Plats.remove(hasPlat);
	},
})