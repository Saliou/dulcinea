CategoriesPlat = new Meteor.Collection("categoriePlat");

Meteor.methods({

	addCategoriePlat : function( categoriePlat )
	{
		/*check(categoriePlat,
		{
			CatPla_nom : String,
		})*/

		var categoriePlatExist = CategoriesPlat.findOne({CatPla_nom : categoriePlat.CatPla_nom});

		if( categoriePlatExist ){ return "categoriePlat_exist" }
		
		return CategoriesPlat.insert( categoriePlat );
	},

	editCategoriePlat : function( categoriePlat )
	{
		
	},

	deleteCategoriePlat : function( categoriePlatId )
	{
		check(categoriePlatId,String);

		var hasCategoriePlat = CategoriesPlat.findOne({_id : categoriePlatId});

		if( !hasCategoriePlat ){ return "categoriePlat_doesnt_exist";}

		return CategoriesPlat.remove(hasCategoriePlat);
	},
})