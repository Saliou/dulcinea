Meteor.methods({

	totalCommandeTva : function  ( plats )
	{
		//TVA Alcool 20% tout le reste 10%

		var price = 0;

		var tvaA = 20; // TVA Alcool
		
		var tvaN = 10; // TVA Nourriture

		var alcool = CategoriesPlat.findOne({CatPla_nom : "alcool"});

		for (var i = 0; i < plats.length; i++)
		{
			if( plats[i].Pla_categorie == alcool._id )
			{
				var tva = tvaA;
			}
			else
			{
				var tva = tvaN;
			}
			
			var tvaToAdd = 	((plats[i].Pla_prix )/100) * tva;

			tvaToAdd =  Math.round(tvaToAdd * 100) / 100;
			
			price += ( plats[i].Pla_prix + tvaToAdd ) * plats[i].Pla_quantity;

		}
		return price;
	}
})

	